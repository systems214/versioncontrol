#ifndef _helper_h
#define _helper_h
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<openssl/sha.h>
#include <sys/types.h>
#include <dirent.h>
#include<sys/io.h>
#include<pthread.h>
#include<sys/mman.h>
#include"../server_dir/server.h"
char* read_file(char*);
char* convert_sha(char*);
DIR* open_project(char* project);
char* read_file_mmap(char*);
void delete_project(DIR*);
struct manifest * update_hash(char*,struct manifest*);
struct manifest * load_manifest(int,char*);
char* generate_live_hash(char*,char*);
struct entry {
	int version;
	char* hash;
	char* path;
};

struct manifest {
	int size;
	int high_ver;
	struct entry ** man_list;
};

struct node {
	char tag;
	int version;
	char* path;
	struct node * next;
};

#endif
