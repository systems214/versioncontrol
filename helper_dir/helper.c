
#include"helper.h"


char* convert_sha(char* str) {
	if(str == NULL) {
		return NULL;
	}

	char * buffer = (char*) malloc(sizeof(char)*SHA_DIGEST_LENGTH*2 + 1) ;
	memset(buffer, '\0', (SHA_DIGEST_LENGTH*2+1));

	unsigned char * sha;
	sha = (unsigned char *) malloc(sizeof(char)*SHA_DIGEST_LENGTH+1);

	memset(sha, '\0', SHA_DIGEST_LENGTH+1);

	SHA1((unsigned char*)str, strlen(str), sha);

	int i = 0;
	for(i = 0; i < SHA_DIGEST_LENGTH; i++) {
		sprintf((char*)&(buffer[i*2]), "%02x", sha[i]);
	}

	free(sha);
	return buffer;
}

char* read_file_mmap(char* path) {
	int size = 0;
	int fd = 0;
	struct stat s;
	int status = 0;
	fd = open(path, O_RDONLY);
	if(fd < 1) {
		return NULL;
	}
	status = fstat(fd, &s);
	size = s.st_size;
	if(size == 0) {
		return NULL;
	}
	char* f = (char *) mmap(NULL,size,PROT_READ, MAP_SHARED, fd,0);
	close(fd);
	return f;
}

char* generate_live_hash(char* path, char* hash) {
	char* str = NULL;
	str = read_file_mmap(path);
	unsigned char buf[SHA_DIGEST_LENGTH+1];
	SHA1((unsigned char *)str,strlen(str),buf);
	int i = 0;
	for(i = 0; i < SHA_DIGEST_LENGTH;i++) {
		sprintf((char*)&(hash[i*2]), "%02x", buf[i]);
	}

	return hash;
}


DIR* open_project(char * project) {
	if(project == NULL) {
		return NULL;
	}
	char* path = NULL;
	int size = strlen(project)+3;
	path = (char*) malloc(sizeof(char)*size);
	memset(path, '\0', size);

	strcat(path,"./");
	strcat(path,project);

	DIR* dirp = opendir(path);

	if(dirp == NULL) {
		free(path);
		return NULL;
	}
	free(path);
	return dirp;
}

void delete_project(DIR* dir){
	struct dirent* dirent;
	if((dirent = readdir(dir)) == NULL) return;

	char path[strlen(dirent->d_name)+3];
	memset(path, '\0', strlen(dirent->d_name)+3);
	strcat(path, "./");
	strcat(path, dirent->d_name);

	if(dirent->d_type == DT_DIR) delete_project(dir);

	remove(path);
	delete_project(dir);
}

//need to add protoype
//need to free man_list after, or make function that frees these types of shit
struct manifest * load_manifest(int type,char* project) {
	//client version
	char path[1024];
	memset(path,'\0', 1024);
	if(type == 1) {
		strcat(path, "client_dir/");
		strcat(path, project);

	}
	else {
		strcat(path, "server_dir/");
		strcat(path, project);
	}



	strcat(path,"/.manifest");
	FILE* fd = NULL;
	fd = fopen(path,"r");
	if(fd == NULL) {
		printf("Manifest was not found\n");
		return NULL;
	}

	int size = 0;
	int i = 0;
	int index = 0;
	int ver = 0;
	char* hash = NULL;
	char* man_path = NULL;
	struct manifest * manifest = (struct manifest *) malloc(sizeof(struct manifest));
	fscanf(fd,"%d %d\n",&manifest -> size, &manifest -> high_ver);
	if(manifest -> size == 0) {
		manifest -> man_list = NULL;
		fclose(fd);
		return manifest;
	}


	size = manifest -> size;
	struct entry ** man_list = (struct entry **) malloc(sizeof(struct entry*)*size);
	struct entry * entri = NULL;
	for(i = 0; i < size; i++) {
		man_list[i] = NULL;
	}
	manifest -> man_list = man_list;

	while(index < size) {
		hash = (char*) malloc(sizeof(char)*41);
		memset(hash, '\0', 41);
		man_path = (char*) malloc(sizeof(char)*256);
		memset(man_path,'\0',256);
		fscanf(fd,"%d\t%s\t%s\n", &ver, hash, man_path);
		entri = (struct entry*) malloc(sizeof(struct entry));
		entri -> version = ver;
		entri -> hash = hash;
		entri -> path = man_path;
		man_list[index] = entri;
		index++;
	}
	fclose(fd);
	return manifest;
}

struct manifest * update_hash(char* project, struct manifest * manifest) {
	int i = 0;
	char* path = NULL;
	struct entry ** man_list = NULL;
	struct entry * entri = NULL;
	char* new_hash = NULL;
	man_list = manifest -> man_list;
	while( man_list[i] != NULL ) {
		entri = man_list[i];
		path = entri -> path;
		free(entri -> hash);
		entri -> hash = NULL;
		entri -> hash = convert_sha(read_file_mmap(path));
		i++;
	}
	//delete the old .manifest, then insert new one using this man_list
	return manifest;
}

void free_manifest(struct manifest* manifest) {
	int i = 0;
	if(manifest == NULL) {
		return;
	}
	if(manifest -> man_list == NULL) {
	}
	else {
		for(i = 0; i < manifest -> size; i++) {
		struct entry * entry = manifest -> man_list[i];
		if(entry != NULL) {
			free(entry -> path);
				free(entry -> hash);
				free(entry);
				manifest -> man_list[i] = NULL;
			}
		}
	}

	if(manifest -> man_list != NULL) free(manifest -> man_list);
	if(manifest != NULL) free(manifest);
}
