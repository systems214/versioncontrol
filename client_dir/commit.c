#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#include"client.h"
#include"connect_client.h"
#include"commit.h"
#include"../helper_dir/helper.h"

int commit(char* project){

	CLIENT(project);

	char update_path[256] = {0};
	strcpy(update_path, path);
	strcat(update_path, "/.update");
	FILE* update_file = fopen(update_path, "r");

	if(update_file != NULL){
		char c = '\0';
		if(fscanf(update_file, "%c", &c) != 0){
			printf(".update file is not empty\n");
			fclose(update_file);
			return -1;
		}
	}

	int sock = 0;

	if(connect_client(&sock) == -1){
		return -1;
	}

	char message[256] = {0};
	strcpy(message, "com:");
	strcat(message, project);
	send(sock, message, strlen(message), 0);

	//read in server version
	int server_version = 0;
	int value = recv(sock, (unsigned char*)&server_version, sizeof(int), 0);

	if(server_version == -1){
		printf("Project DNE\n");
		return -1;
	}

	char manifest_path[256] = {0};
	strcpy(manifest_path, path);
	strcat(manifest_path, "/.manifest");

	FILE* manifest_file = fopen(manifest_path, "r");

	if(manifest_file == NULL){
		printf("Client manifest not found\n");
		close(sock);
		return -1;
	}

	int num_files = 0;
	int client_version = 0;
	fscanf(manifest_file, "%d %d\n", &num_files, &client_version);

	if(server_version != client_version){
		printf("server and client versions do not match. Run update.\n");
		send(sock, "version diff\0", strlen("version diff")+1, 0);
		fclose(manifest_file);
		close(sock);
		return -1;
	}

	fclose(manifest_file);

	struct manifest * manifest = (struct manifest*) malloc(sizeof(struct manifest));
	manifest = load_manifest(1,project);
	int i;
	for(i = 0; i < manifest -> size; i++) {
		char entry_path_for_live[1024] = {0};
		sprintf(entry_path_for_live,"client_dir/%s/%s",project,manifest -> man_list[i] -> path);
		manifest -> man_list[i] -> hash = generate_live_hash(entry_path_for_live,manifest -> man_list[i] -> hash);
	}

	remove(manifest_path);

	//This is where the manifest file is being written
	manifest_file = fopen(manifest_path,"w+");

	fprintf(manifest_file,"%d %d\n",manifest -> size, manifest -> high_ver);
	for(i = 0; i < manifest -> size; i++) {
		struct entry * temp_entry = manifest -> man_list[i];
		if(temp_entry == NULL) {
			continue;
		}
		fprintf(manifest_file,"%d\t%s\t%s\n",temp_entry ->  version, temp_entry -> hash, temp_entry -> path);
	}
	free_manifest(manifest);
	manifest = NULL;
	fclose(manifest_file);

	manifest_file = fopen(manifest_path,"r");

	fscanf(manifest_file, "%d %d\n", &num_files, &client_version);


	send(sock, "version same\0", strlen("version same")+1, 0);

	char file_path[1024] = {0};
	int file_version = 0;
	char curr_hash[41] = {0};

	char* commit_buffer[num_files];

	for(i=0; i < num_files; i++){
		commit_buffer[i] = NULL;
	}

	//read number of server files
	int num_server = 0;
	value = recv(sock, (unsigned char*)&num_server, sizeof(int), 0);

	char server_manifest[num_server][1024];
	for(i=0; i<num_server; i++){
		memset(server_manifest[i], '\0', 1024);
		int buf_size = 0;
		value = recv(sock, (unsigned char*)&buf_size, sizeof(int), 0);
		value = recv(sock, server_manifest[i], buf_size, 0);
	}
	int buf_size = 308;
	int found[num_server];
	memset(found, 0, num_server);
	int l = 0;
	i=0;
	int a = 0;
	int k=0;
	while(fscanf(manifest_file, "%d\t%s\t%s\n", &file_version, curr_hash, file_path) != EOF){
		int s_file_version = 0;
		char server_hash[41];
		server_hash[40] = '\0';
		char server_path[256];
		memset(server_path, '\0', 256);
		char version_buf[10] = {0};
		int pass = 0;

		a++;
		int j;
		for(j=0; j < num_server; j++){
			if(sscanf(server_manifest[j], "%d\t%s\t%s\n", &s_file_version, server_hash, server_path) != EOF){

				if(!(s_file_version > file_version) && strcmp(server_hash, curr_hash) != 0 && strcmp(server_path, file_path) == 0){
					sprintf(version_buf, "%d", file_version+1);
					found[l] = j;
					commit_buffer[j] = (char*) malloc(sizeof(char)*buf_size);
					sprintf(commit_buffer[j], "%d\t%s\t%s\n", file_version+1, curr_hash, file_path);
					pass = 1;
					k++;
					l++;
					break;
				}else if((s_file_version > file_version) && strcmp(server_path, file_path) == 0){
					printf("Server version greater at project %s:\nAborting\n", server_path);
					fclose(manifest_file);
					close(sock);
					return -1;
				}else if(strcmp(server_hash, curr_hash) == 0 && strcmp(server_path, file_path) == 0){
					pass = 1;
					found[l] = j;
					l++;
					break;
				}
			}
		}
		if(pass != 1){
			sprintf(version_buf, "%d", file_version);
			commit_buffer[k] = (char*) malloc(sizeof(char)*buf_size);
			sprintf(commit_buffer[k], "%d\t%s\t%s\n", file_version, curr_hash, file_path);
			pass = 0;
			k++;
		}
		i++;
	}

	fclose(manifest_file);
	if(i == 0){
		printf("No commits to be made. No files have been changed\n");
		send(sock, "no change\0", strlen("no change")+1, 0);
		close(sock);
		return -1;
	}

	int j;
	for(j=0; j<l; j++){
		int m;
		for(m=j; m<l; m++){
			if(m == found[j]){
				j++;
				continue;
			}
			commit_buffer[k] = (char*) malloc(sizeof(char)*buf_size);
			sscanf(server_manifest[m], "%*d\t%s\t%s\n", curr_hash, file_path);
			sprintf(commit_buffer[k], "%d\t%s\t%s\n", -1, curr_hash, file_path);
			k++;
			j++;
			a--;
			break;
		}
	}

	char rm[256] = {0};
	sprintf(rm, "rm %s/.commit*", path);
	system(rm);

	send(sock, "is change\0", strlen("is change")+1, 0);
	int commit_num;
	value = recv(sock, (char*)&commit_num, sizeof(int), 0);

	char commit_path[1024] = {0};
	sprintf(commit_path, "%s%s%d", path, "/.commit", commit_num);
	FILE* commit_file = fopen(commit_path, "w+");

	fprintf(commit_file, "%d %d\n", k, ++client_version);

	char* commit_stream = NULL;
	char* temp;
	int stream_size = 0;
	for(j=0; j<k; j++){
		if(commit_buffer[j] == NULL) break;
		fprintf(commit_file, "%s", commit_buffer[j]);
		temp = commit_stream;
		commit_stream = (char*) malloc(sizeof(char)*(stream_size + (strlen(commit_buffer[j])+1)));
		if(temp == NULL){
			strcpy(commit_stream, commit_buffer[j]);
			stream_size = strlen(commit_buffer[j]);
			continue;
		}
		strcpy(commit_stream, temp);
		strcat(commit_stream, commit_buffer[j]);
		stream_size = strlen(commit_stream);
		free(temp);
	}

	fclose(commit_file);

	if(commit_buffer[0] == NULL){
		printf("Commit buffer empty. Project has been poisoned. Rollback and upgrade to recover the project\n");
		return -1;
	}

	manifest = (struct manifest*) malloc(sizeof(struct manifest));
	manifest = load_manifest(1,project);
	for(j = 0; j < k; j++) {
		int entry_version = -1;
		char hash[1024] = {0};
		char entry_path[1024] = {0};
		sscanf(commit_buffer[j],"%d\t%s\t%s\n", &entry_version, hash, entry_path);
		if(entry_version > 0) {
			for(i = 0;i < manifest -> size; i++) {
				if(manifest -> man_list[i] == NULL) {
					continue;
				}
				if(strcmp(manifest -> man_list[i] -> path, entry_path) == 0) {
					strcpy(manifest -> man_list[i] -> hash, hash);
					manifest -> man_list[i] -> version = entry_version;
				}
			}
		}
	}
	remove(manifest_path);

	//This is where the manifest file is being written
	manifest_file = fopen(manifest_path,"w+");

	fprintf(manifest_file,"%d %d\n",manifest -> size, manifest -> high_ver);
	for(i = 0; i < manifest -> size; i++) {
		struct entry * temp_entry = manifest -> man_list[i];
		if(temp_entry == NULL) {
			continue;
		}
		fprintf(manifest_file,"%d\t%s\t%s\n",temp_entry ->  version, temp_entry -> hash, temp_entry -> path);
	}
	free_manifest(manifest);
	manifest = NULL;
	fclose(manifest_file);

	for(j=0; j<i; j++){
		free(commit_buffer[j]);
	}
	int commit_len = strlen(commit_stream);
	value = send(sock, (char*)&k, sizeof(int), 0);
	value = send(sock, (char*)&(client_version), sizeof(int), 0);
	value = send(sock, (char*)&commit_len, sizeof(int), 0);
	value = send(sock, commit_stream, strlen(commit_stream), 0);

	char reply[strlen("not receive")+1];
	memset(reply, '\0', strlen("not receive")+1);
	value = recv(sock, reply, strlen("not receive")+1, 0);
	if(strcmp(reply, "not receive") == 0){
		char delete[256] = {0};
		strcpy(delete, "rm ");
		strcat(delete, commit_path);
		system(delete);
		printf("server did not receive .commit. Commit to try again\n");
	}
	free(commit_stream);
	close(sock);
	return 0;
}
