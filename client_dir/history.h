#ifndef _history_h
#define _history_h
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<netinet/in.h>
#include"../helper_dir/helper.h"
#include"client.h"
#include"connect_client.h"

int history(char*);
#endif
