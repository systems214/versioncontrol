#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<netinet/in.h>
#include"create.h"
#include"client.h"
#include"connect_client.h"

int create(char* project){
	
	//socket. value not needed to be declared here but w.e
	int sock = 0, value;

	//connect client. give socket pointer
	if(connect_client(&sock) == -1){
		return -1;
	}

	//send command to server
	send(sock, "cre:", strlen("cre:"), 0);
	send(sock, project, strlen(project), 0);

	//create project on client side
	printf("Creating project...");

	char buffer[strlen("Project created")+1];
	memset(buffer, '\0', strlen("Project created")+1);

	value = recv(sock, buffer, strlen("Project created"), 0);

	if(strcmp(buffer, "Project created") == 0){
		char cmd[256];
		sprintf(cmd, "%s", "mkdir ");
	
		strcat(cmd, "client_dir/");
		strcat(cmd, project);

		system(cmd);
		
		CLIENT(project);
		strcat(path, "/.manifest");
printf("%s\n", path);
		FILE* manifest = fopen(path, "w");
		fwrite((void*) "0 0\n", 1, strlen("0 0\n"), manifest), 
		printf("Project created\n");
		
		close(sock);
		return 0;
	}else{
		printf("Project exists\n");
		close(sock);
		return -1;
	}
}
