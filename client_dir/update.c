#include"update.h"

int update(char* project){
	int sock = 0, value;

	if(connect_client(&sock) == -1) {
		printf("Return 2\n");
		return -1;
	}


	char upd_buffer[256];
	memset(upd_buffer, '\0', 256);
	strcat(upd_buffer,"upd:");
	strcat(upd_buffer,project);

	send(sock, upd_buffer, 256, 0);
	printf("Updating Project...\n");

	char file_size_buffer[4]; //create a file buffer where the int is going to be received
	memset(file_size_buffer,'\0',4);

	value = read(sock, file_size_buffer, sizeof(int)); //read in the bytes
	int file_size = * (int*) file_size_buffer; // convert to int
	if(file_size < 0){
		printf("No response from server.\n");
		return -1;
	}
	else {

		if(file_size == -1){
			printf("Project doesn't exists on server.\n");
			return -1;
		}

		if(file_size != 0) {

			char buffer[file_size+1];
			char *data = buffer;
			recv(sock,buffer,file_size,0);
			int offset = 0;
			int ver = 0;
			char* hash = NULL;
			char* file_path = NULL;
			int size = 0;
			int index = 0;
			struct manifest * manifest = (struct manifest*) malloc(sizeof(struct manifest));
			sscanf(data,"%d %d\n%n", &size, &ver,&offset) +2;
			data = data + offset;
			manifest -> size = size;
			manifest -> high_ver = ver;

			if(size != 0) {
				struct entry ** man_list = (struct entry**) malloc(sizeof(struct entry*)*size);
				int i = 0;
				for(i = index; i < size; i++) {
					man_list[i] = NULL;
				}
				struct entry * man_entry = NULL;

				manifest -> man_list = man_list;

				while( index < size) {
					hash = (char*) malloc(sizeof(char)*41);
					file_path = (char*) malloc(sizeof(char)*1025);
					memset(file_path,'\0',1025);
					memset(hash,'\0',41);
					sscanf(data,"%d\t%s\t%s\n%n",&ver, hash, file_path,&offset);
					data = data + offset;
					man_entry = (struct entry*) malloc(sizeof(struct entry));
					man_entry -> version = ver;
					man_entry -> hash = hash;
					man_entry -> path = file_path;
					man_list[index] = man_entry;
					index++;
				}
			}
			else {
				manifest -> man_list = NULL;
			}

			struct manifest * local_man = load_manifest( (int) 1,project);

			int i = 0;
			int j = 0;
			int conflict = 0;

			struct entry * local_entry = NULL;
			struct entry * man_entry = NULL;

			if( (local_man -> high_ver) == (manifest -> high_ver )) {
				char update_path_blah[1024] = {0};
				sprintf(update_path_blah,"client_dir/%s/.update",project);
				FILE* update_file = fopen(update_path_blah,"w+");
				fclose(update_file);
				free_manifest(manifest);
				free_manifest(local_man);
				printf("Up to date\n");
				return 1;
			}
			else {
			unsigned char * live_hash = (unsigned char *) malloc(sizeof(unsigned char*)*41);
			memset(live_hash, '\0', 41);


			//manifest versions are different
				if(manifest -> man_list != NULL && local_man -> man_list != NULL) {
					for(i = 0; i < local_man -> size; i++) {
						local_entry = local_man -> man_list[i];
						for(j = 0; j < manifest -> size; j++) {
							man_entry = manifest -> man_list[j];
							if (strcmp(local_entry -> path, man_entry -> path) == 0) {
								if( local_entry -> version != man_entry -> version) {
									CLIENT(project);
									strcat(path,"/");
									strcat(path,local_entry -> path);
									live_hash = generate_live_hash(path, live_hash);
									if(strcmp(man_entry -> hash, live_hash) != 0) {
										if((local_entry -> hash, live_hash) != 0){
											fprintf(stdout,"Conflcit: %d\t%s\t%s\n",local_entry -> version, local_entry -> hash, local_entry -> path);
											conflict = 1;
										}
									}
								}
							}
						}
					}
				}


				if(conflict == 1) {
					free(live_hash);
					free_manifest(local_man);
					free_manifest(manifest);
					close(sock);
					return -1;
				}

				//Create update file
				FILE* update_fp = NULL;
				CLIENT(project);
				strcat(path,"/.update");
				update_fp = fopen(path,"w");
				conflict = -1; //now will be used to see if dupes were found;



				//Both manifests have something in them
				if(manifest -> man_list != NULL && local_man -> man_list != NULL) {
					//Loop through both manifests
					for(i = 0; i < local_man -> size; i++) {
						//grab an entry from the local manifest
						local_entry = local_man -> man_list[i];
						conflict = -1;
						//Reset for any matching files flag
						for(j = 0; j < manifest -> size; j++) {
							//grab entry from server's manifest
							man_entry = manifest -> man_list[j];

							//check if entry is null, continue if so
							if(man_entry == NULL) {
								continue;
							}

							//check if there is a file match
							if(strcmp(local_entry -> path, man_entry -> path) == 0) {
								char live_path[1024];
								memset(live_path,'\0',1024);
								strcat(live_path,"client_dir/");
								strcat(live_path,project);
								strcat(live_path,"/");
								strcat(live_path,local_entry -> path);
								live_hash = generate_live_hash(live_path, live_hash);
								//record where in the server's manifest that the file matched, which should happen once
								conflict = j;
								if(local_entry -> version == man_entry -> version) {
									//this is an upload, so and other test case, so remove

									//free the two entries in both lists
									free(local_entry -> hash);
									free(local_entry -> path);
									free(local_entry);
									local_entry = NULL;
									local_man -> man_list[i] = NULL;
									free(man_entry -> hash);
									free(man_entry -> path);
									free(man_entry);
									man_entry = NULL;
									manifest -> man_list[j] = NULL;
									break;

								}
								else {
									if(strcmp(live_hash, man_entry -> hash) == 0) {
										//write to .update that it is modified
										fprintf(update_fp,"M\t%d\t%s\t%s\n",man_entry -> version ,man_entry -> hash, man_entry -> path);
										fprintf(stdout,"M\t%d\t%s\t%s\n",man_entry -> version,man_entry -> hash, man_entry -> path);

										//free the two entries in both lists
										free(local_entry -> hash);
										free(local_entry -> path);
										free(local_man -> man_list[i]);
										local_entry = NULL;
										local_man -> man_list[i] = NULL;
										free(man_entry -> hash);
										free(man_entry -> path);
										free(manifest -> man_list[j]);
										man_entry = NULL;
										manifest -> man_list[j] = NULL;
										break;
									}
								}
							}
						}
						//if the local_entry is null, then we already written, so continue to next entry in our local_man
						if(local_entry == NULL) {
							continue;
						}
						//if the file was not found in the server's manifest, that means it was deleted, print it
						if(conflict < 0) {
							fprintf(update_fp,"D\t%d\t%s\t%s\n",local_entry -> version ,local_entry -> hash, local_entry -> path);
							fprintf(stdout,"D\t%d\t%s\t%s\n",local_entry -> version,local_entry -> hash, local_entry -> path);

							if(local_entry -> hash != NULL) free(local_entry -> hash);
							if(local_entry -> path != NULL) free(local_entry -> path);
							if(local_entry != NULL) free(local_entry);
							local_entry = NULL;
							local_man -> man_list[i] = NULL;
						}
					}

					for(j = 0; j < manifest -> size; j++) {
						if(manifest -> man_list[j] != NULL) {
							man_entry = manifest -> man_list[j];
						}
						else {
							continue;
						}
						fprintf(update_fp,"A\t%d\t%s\t%s\n",man_entry -> version,man_entry -> hash, man_entry -> path);
						fprintf(stdout,"A\t%d\t%s\t%s\n",man_entry -> version,man_entry -> hash, man_entry -> path);
						free(man_entry -> hash);
						free(man_entry -> path);
						free(man_entry);
						man_entry = NULL;
						manifest -> man_list[j] = NULL;
					}
				}
				//Incoming manifest has nothing in it, meaning everything was deleted
				if(manifest -> man_list == NULL) {
					for(i = 0; i < local_man -> size; i++) {
						local_entry = local_man -> man_list[i];
						if(local_entry == NULL) {
							continue;
						}
						fprintf(update_fp,"D\t%d\t%s\t%s\n",local_entry -> version,local_entry -> hash, local_entry -> path);
						fprintf(stdout,"D\t%d\t%s\t%s\n",local_entry -> version,local_entry -> hash, local_entry -> path);
					}
				}
				//our manifest has nothing in it, add in everything from server's manifest
				else {
					for(i = 0; i < manifest -> size; i++) {
						man_entry = manifest -> man_list[i];
						if(man_entry == NULL) {
							continue;
						}
						fprintf(update_fp,"A\t%d\t%s\t%s\n",man_entry -> version,man_entry -> hash, man_entry -> path);
						fprintf(stdout,"A\t%d\t%s\t%s\n",man_entry -> version,man_entry -> hash, man_entry -> path);

					}
				}
				free(live_hash);
				fclose(update_fp);
				free_manifest(local_man);
				free_manifest(manifest);

			}//end of manifest versions are different
		}
	}
	return 1;
}
