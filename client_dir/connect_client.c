#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include"connect_client.h"

int connect_client(int* sock){
	
	struct sockaddr_in server_address;
	*sock = socket(AF_INET, SOCK_STREAM, 0);

	if(*sock < 0){
		perror("Client socket failed");
		return -1;
	}
	
	int opt = 1;
	setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));

	FILE* conf_file = fopen("./client_dir/.wtf/.configure", "r");

	if(conf_file == NULL){
		printf("No .configure found\n");
		return -1;
	}

	char ip[16];
	ip[16] = '\0';
	int port;

	fscanf(conf_file, "%s\n%d", ip, &port);
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);

	if(inet_pton(AF_INET, ip, &server_address.sin_addr) <= 0){
		perror("Invalid address");
		fclose(conf_file);
		return -1;
	}

	while(1){
		if(connect(*sock, (struct sockaddr*)&server_address, sizeof(struct sockaddr_in)) < 0){
			perror("Connection Failed. Trying again in 3 seconds");
			sleep(3);
		}else{
			break;
		}
	}
	fclose(conf_file);
	return 0;

}
