#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include"currentversion.h"

int currentversion(char* project){
	
	int sock = 0, value;
	

	if(connect_client(&sock) == -1){
		return -1;
	}

	char buffer[256];
	memset(buffer, '\0', 256);
	
	strcat(buffer, "cur:");
	strcat(buffer, project);

	send(sock, buffer, 256, 0);
	
	int project_version = 0;
	read(sock, (char*)&project_version, sizeof(int));

	printf("Retrieving project version\n");

	char _message_len[4];
	
	value = recv(sock, _message_len, sizeof(int), 0);
	
	int message_len = (int) *_message_len;
	
	if(message_len == -1){
		printf("Project DNE\n");
		return -1;
	}
	if(message_len == 0){
		printf("Project has no files. Current Version: 0\n");
		return -1;
	}

	char message[message_len+1];
	memset(message, '\0', message_len+1);
	value = recv(sock, message, message_len, 0);
	
	printf("\nProject Version of %s: %d\n", project, project_version);	
	printf("%s\n", message);

	close(sock);
	return 0;
}
