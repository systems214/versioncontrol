#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<errno.h>
#include<fcntl.h>
#include<unistd.h>
#include"connect_client.h"
#include"client.h"
#include"rollback.h"

int rollback(char* project, int version){

	int sock = 0, value;

	if(connect_client(&sock) == -1){
		return -1;
	}

	char command[strlen("rol:") + strlen(project) + 1];
	memset(command, '\0', strlen("rol:") + strlen(project) +1);
	sprintf(command, "rol:%s", project);

	send(sock, command, strlen(command), 0);

	char buffer[4] = {0};
	recv(sock, buffer, 3, 0);

	send(sock, (char*)&version, sizeof(int), 0);

	recv(sock, buffer, 3, 0);

	if(strcmp(buffer, "suc") == 0){
		printf("Rollback success\n");
	}else{
		printf("Rollback failed\n");
	}
	close(sock);

	return -1;
}
