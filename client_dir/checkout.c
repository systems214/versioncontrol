#include "checkout.h"

int checkout(char* project) {
	int sock;

	char path[1024] = {0};
	sprintf(path,"client_dir/%s/.manifest",project);
	FILE* fp = fopen(project,"r");
	if(fp != NULL) {
		printf("Project already exists in client side\n");
		return -1;
	}

	if(connect_client(&sock) == -1) {
		return -1;
	}
	char buf[1024] = {0};
	sprintf(buf,"chk:%s",project);
	send(sock,buf,(4+strlen(project)),0);


	//read the history file size;
	int size = 0;
	char* size_buffer[4] = {0};
	read(sock,size_buffer,4);
	size = * (int*) size_buffer;
	if(size == -1) {
		printf("Project is not found\n");
		close(sock);
		return 1;
	}
	//this has the history file
	char buffer[size];
	read(sock,buffer,size);
	char cmd[1024] = {0};
	FILE * temp = fopen("client_dir/package.tar.gz","w+");
	fwrite(buffer,1,size,temp);
	fclose(temp);
	system("tar -C client_dir -zxvf client_dir/package.tar.gz");
	sprintf(cmd, "client_dir/%s/history",project);
	remove(cmd);
	remove("client_dir/package.tar.gz");
	close(sock);
	return 1;
}
