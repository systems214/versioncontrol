#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"add.h"
#include <sys/types.h>
#include <dirent.h>
#include"../helper_dir/helper.h"
#include"client.h"

int add(char * project, char* filename) {
	if(filename == NULL) {
		printf("Invalid filename\n");
		return -1;
	}

	char file_path[1024];
	memset(file_path, '\0', 1024);
	strcat(file_path,"client_dir/");
	strcat(file_path,project);
	strcat(file_path,"/");
	strcat(file_path,filename);
	char* str = NULL;
	str = read_file_mmap(file_path);
	if(str == NULL) {
		printf("File could not be found\n");
		return -1;
	}
	char* hash = NULL;
	hash = convert_sha(str);
	
	struct manifest * manifest = load_manifest(1,project);
	if(manifest == NULL) {
		printf("No manifest was found\n");
	}
	else {
		int i = 0;
		for(i = 0; i < manifest -> size; i++) {
			if(strcmp(manifest -> man_list[i] -> path, filename) == 0) {
				printf("File was already added\n");
				free_manifest(manifest);
				free(hash);
				return 1;
			}
		}
		CLIENT(project);
		strcat(path,"/.manifest");
		FILE *fp = fopen(path, "w");
		if(fp == NULL) {
			printf("Opening to write .manifest failed\n");
		}
		else {
			fprintf(fp, "%d %d\n", (manifest -> size +1), manifest -> high_ver);
			for(i = 0; i < manifest -> size; i++) {
				struct entry * temp_entry = manifest -> man_list[i];
				fprintf(fp, "%d\t%s\t%s\n", temp_entry -> version, temp_entry -> hash, temp_entry -> path);
			}
			fprintf(fp, "%d\t%s\t%s\n", 0, hash, filename);
		}
		fclose(fp);
		free_manifest(manifest);
	}
	free(hash);
	printf("Successfully added\n");
	return 1;
}

