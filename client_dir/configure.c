#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<string.h>
#include"configure.h"

int configure(char* IP, int port){
	
	struct sockaddr_in address;
	int sock = 0, value;

	struct sockaddr_in server_address;

	char message[25];
	memset(message, '\0', 25);
	strcat(message,"con:");
	strcat(message, IP);
	
	char buffer[30];
	memset(buffer, '\0', 30);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0){
		printf("No socket\n");
		return -1;
	}

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);

	if(inet_pton(AF_INET, IP, &server_address.sin_addr) <= 0){
		printf("Invalid Address\n");
		return -1;
	}

	if(connect(sock, (struct sockaddr*)&server_address, sizeof(struct sockaddr_in)) < 0){
		printf("Connection Failed\n");
		return -1;
	}
	
	
	send(sock, message, strlen(message), 0);

	printf("Configuring IP: %s\n", IP);
	
	value = read(sock, buffer, 30);
	
	if(value > 0){
		printf("%s\n", buffer);
	}else{
		printf("Configure failed\n");
		return -1;
	}
	return 0;
}
