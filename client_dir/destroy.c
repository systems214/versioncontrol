#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<errno.h>
#include<dirent.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<netinet/in.h>
#include"../helper_dir/helper.h"
#include"destroy.h"

int destroy(char* project){

	struct sockaddr_in address;
	int sock = 0, value;

	struct sockaddr_in server_address;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sock < 0){
		perror("client socket failed");
		return -1;
	}
	
	int client_fd = open("./client_dir/.wtf/.configure", O_RDONLY, 0640);

	if(errno == ENOENT){
		perror("no .configure file found");
		printf("run wtfclient configure <ip> <port>\n");
		return -1;
	}

	char ip[16];
	char _port[6];

	struct stat statbuf;
	fstat(client_fd, &statbuf);
	
	if(statbuf.st_size <= 0){
		printf(".configure empty. run wtfclient configure <ip> <port>\n");
		return -1;
	}

	char* src = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, client_fd, 0);
	
	sscanf(src, "%s%*[\n]%s%*[\n]", ip, _port);

	close(client_fd);

	int port = atoi(_port);
	
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);

	if(inet_pton(AF_INET, ip, &server_address.sin_addr) <= 0){
		perror("invalid address");
		return -1;
	}

	if(connect(sock, (struct sockaddr*)&server_address, sizeof(struct sockaddr_in)) < 0){
		perror("connection failed");
		return -1;
	}

	send(sock, "des:", strlen("des:"), 0);
	send(sock, project, strlen(project), 0);

	printf("Destroying Project...\n");

	char inc_message[strlen("Project Destroyed")+1];
	memset(inc_message, '\0', strlen("Project Destroyed")+1);
	
	value = read(sock, inc_message, strlen("Project Destroyed")+1);

	if(value == 0){
		printf("Empty response from server\n");
		return -1;
	}else{
		printf("Project destroyed on server\n");
		return 0;
	}
	close(sock);

	return -1;
}



