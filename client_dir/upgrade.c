#include "upgrade.h"

int upgrade_client(char* project) {
	int sock;
	if(connect_client(&sock) == -1) {
		return -1;
	}

	char confirm;
	FILE* fp = NULL;
	CLIENT(project);
	strcat(path,"/.update");
	fp = fopen(path,"r");
	if(fp == NULL) {
		printf("No .update file exists, please execute update first\n");
		return -1;
	}
	else {
		fseek(fp,0,SEEK_END);
		int size = ftell(fp);
		if(size == 0) {
			fclose(fp);
			char cmd[256];
			sprintf(cmd, "%s", "rm ");
			strcat(cmd, path);
			if(system(cmd) != -1) {
				printf("Already up to date\n");
			}
			else {
				printf("Something went wrong, cannot delete .update\n");
			}
			return -1;
		}
		fseek(fp,0,SEEK_SET);
	}

	char value;
	char file_path[1024];
	int ver = -1;
	memset(file_path,'\0',1024);
	char hash[41];
	memset(hash,'\0',41);
	struct manifest * manifest = NULL;
	manifest = load_manifest(1, project);
	if(manifest == NULL) {
		printf("Failed to load manifest\n");
		fclose(fp);
		char cmd[256];
		sprintf(cmd,"%s", "rm ");
		strcat(cmd, path);
		if(system(cmd) == -1) {
			printf("Something went wrong, cannot delete .update\n");
		}
		return -1;
	}

	char upd_buffer[256];
	memset(upd_buffer, '\0', 4);
	strcat(upd_buffer, "upg:");
	strcat(upd_buffer, project);
	send(sock, upd_buffer, 4+strlen(project), 0);


	memset(upd_buffer, '\0', 4);
	read(sock, upd_buffer, 1);
	if(upd_buffer[0] == '0') {
		printf("Project does not exists on server\n");
		fclose(fp);
		char cmd[256];
		sprintf(cmd,"%s", "rm ");
		strcat(cmd, path);
		if(system(cmd) == -1) {
			printf("Something went wrong, cannot delete .update\n");
		}

		return -1;
	}

	int i = 0;
	int index_counter = 0;
	struct node * head = (struct node*) malloc(sizeof(struct node));
	struct node * temp = head;
	struct node * prev = NULL;
	int deleted = 0;
	while(fscanf(fp, "%c\t%d\t%s\t%s\n", &value, &ver, hash, file_path) != EOF) {
		if(value == 'D') {
			for(i = 0; i < manifest -> size; i++) {
				if(manifest -> man_list[i] == NULL) {
					continue;
				}
				if( strcmp( manifest -> man_list[i] -> path,file_path) == 0 ) {
					free(manifest -> man_list[i] -> path);
					free(manifest -> man_list[i] -> hash);
					free(manifest -> man_list[i]);
					manifest -> man_list[i] = NULL;
					deleted++;
					continue;
				}
			}
		}

		if(temp == NULL) {
			temp = (struct node *) malloc(sizeof(struct node));
		}
		char * node_path = malloc(sizeof(char)*strlen(file_path));
		strcpy(node_path, file_path);
		temp -> tag = value;
		temp -> version = ver;
		temp -> path = node_path;
		if(prev != NULL) {
			prev -> next = temp;
			temp -> next = NULL;
		}

		prev = temp;
		temp = NULL;
		memset(file_path, '\0', 1024);
		index_counter++;
	}

	//sending the amount of files that need to retrived
	send(sock, (char *) &index_counter, sizeof(int), 0);

	read(sock, &confirm, 1);

	temp = head;
	for(i = 0; i < index_counter; i++) {
		int temp_int = strlen(temp -> path);
		//send length of the name of the file
		send(sock, (char *) &temp_int,sizeof(int),0);
		//send the actual name of the file
		send(sock, temp -> path, strlen(temp -> path), 0);
		temp = temp -> next;
	}


	read(sock, &confirm,1);

	char compressedbuffer[4];
	memset(compressedbuffer,'\0', 4);
	//read the size of the compressed file
	read(sock, compressedbuffer, sizeof(int));
	int size = 0;
	size = * (int*) compressedbuffer;

	send(sock,&confirm, 1, 0);

	//read the file itself
	char buffer[size];
	read(sock, buffer, size);
	char cmd[1024];
	memset(cmd, '\0', 1024);
	sprintf(cmd, "client_dir/%s/package.tar.gz",project);
	FILE* fd = fopen(cmd,"w+");
	fwrite(buffer,sizeof(char), size,fd);
	fclose(fd);
	//decompress file
	memset(cmd,'\0',1024);
	sprintf(cmd, "tar -C client_dir/%s -zxvf client_dir/%s/package.tar.gz ",project, project); //this is going into the client_dir/project directory, then untaring it
	system(cmd);
	//start moving shit
	memset(cmd,'\0',1024);
	sprintf(cmd, "cp -R client_dir/%s/package/* client_dir/%s/", project, project); //this is copying shit from the untared directory (called package),
	//and putting it in the corresponding client_dir
	system(cmd);
	memset(cmd, '\0', 1024);
	sprintf(cmd, "rm -rf client_dir/%s/package",project); //deleting the shit
	system(cmd);
	memset(cmd, '\0', 1024);
	sprintf(cmd, "rm client_dir/%s/package.tar.gz",project);
	system(cmd);
	//write to new manifest at the end (do not change any of the versions tho)

	char number_buf[4];
	memset(number_buf, '\0', 4);
	//read the server's manifest's size
	read(sock,number_buf, sizeof(int));
	size = * (int*) number_buf;



	send(sock,&confirm,1,0);


	char manifest_buffer[size];
	//read the server's manifest itself
	read(sock, manifest_buffer, size);

	//now we have to read the manifest and load it into new manifest
	int server_man_size = 0;
	int server_ver = 0;
	char * data = manifest_buffer;
	int offset = 0;

	struct manifest * server_man = (struct manifest*) malloc(sizeof(struct manifest));
	sscanf(manifest_buffer,"%d %d\n%n",&server_man_size, &server_ver, &offset);
	server_man -> size = server_man_size;
	server_man -> high_ver = server_ver;
	data = data + offset;
	struct entry ** man_list = (struct entry **) malloc(sizeof(struct entry*)*server_man_size);
	int index = 0;
	for(i = index; i < server_man_size; i++) {
		man_list[i] = NULL;
	}
	server_man -> man_list = man_list;
	struct entry * man_entry = NULL;

	while(index < server_man_size ){
		char * man_hash = (char *) malloc(sizeof(char)*41);
		char * man_path = (char *) malloc(sizeof(char)*1025);
		memset(man_path, '\0', 1025);
		memset(hash, '\0', 41);
		sscanf(data, "%d\t%s\t%s\n%n", &ver, man_hash, man_path, &offset);
		data = data + offset;

		man_entry = (struct entry *) malloc(sizeof(struct entry));
		man_entry -> version = ver;
		man_entry -> hash = man_hash;
		man_entry -> path = man_path;
		man_list[index] = man_entry;
		index++;
	}

	int curr_size = manifest -> size;
	int max_size = manifest -> size;
	temp = head;
	while(index_counter > 0) {
		char* file_modify = temp -> path;
		char code = temp -> tag;
		if(code == 'A') {
			if( (curr_size+1) >= max_size) {
				max_size = max_size *2;
				struct entry ** temp_list = (struct entry **) malloc(sizeof(struct entry*)*max_size);
				int counter = 0;
				for(i = 0;i < manifest -> size; i++) {
					temp_list[i] = manifest -> man_list[i];
					counter++;
				}
				for(i = counter; i < max_size; i++) {
					temp_list[i] = NULL;
				}
				free(manifest -> man_list);
				manifest -> man_list = temp_list;
				temp_list = NULL;
			}
			int j = 0;
			for (j = 0; j < server_man -> size; j++) {
				if(strcmp(file_modify, server_man -> man_list[j] -> path) == 0) {
					struct entry * new_entry = (struct entry *) malloc(sizeof(struct entry));
					char* new_path = (char*) malloc(sizeof(char)*1025);
					memset(new_path,'\0',1025);
					char* new_hash = (char*) malloc(sizeof(char)*41);
					memset(new_hash,'\0',41);
					strcpy(new_path, server_man -> man_list[j] -> path);
					strcpy(new_hash, server_man -> man_list[j] -> hash);
					new_entry -> path = new_path;
					new_entry -> hash = new_hash;
					new_entry -> version =  server_man -> man_list[j] -> version;
					manifest -> man_list[manifest -> size] = new_entry;
					manifest -> size++;
					break;
				}
			}
		}
		else {
			struct entry * temp_entry = NULL;
			int j = 0;
			for(j = 0; j < manifest -> size; j++) {
				if(manifest -> man_list[j] == NULL) {
					continue;
				}
				if(strcmp(file_modify, manifest -> man_list[j] -> path) == 0) {
				temp_entry = manifest -> man_list[j];
					break;
				}
			}

			for(j = 0; j < server_man -> size; j++) {
				if(strcmp(file_modify, server_man -> man_list[j] -> path) == 0) {
					strcpy(temp_entry -> path, server_man -> man_list[j] -> path);
					strcpy(temp_entry -> hash, server_man -> man_list[j] -> hash);
					temp_entry -> version = server_man -> man_list[j] -> version;
					break;
				}
			}
		}
		index_counter--;
		temp = temp -> next;
	}
	manifest -> high_ver = server_man -> high_ver;

	memset(cmd,'\0', 1024);
	sprintf(cmd,"rm client_dir/%s/.manifest", project);
	system(cmd);

	memset(cmd, '\0', 1024);
	sprintf(cmd, "client_dir/%s/.manifest", project);
	FILE * fp2 = fopen(cmd, "w+");

	manifest -> size = manifest -> size - deleted;
	fprintf(fp2,"%d %d\n",manifest -> size, manifest -> high_ver);
	for(i = 0; i < manifest -> size; i++) {
		if(manifest -> man_list[i] == NULL) {
			continue;
		}
		int print_ver = manifest -> man_list[i] -> version;
		char * print_hash = manifest -> man_list[i] -> hash;
		char * print_path = manifest -> man_list[i] -> path;
		fprintf(fp2, "%d\t%s\t%s\n",print_ver, print_hash,print_path);
	}
	fclose(fp2);

	while(head != NULL) {
		temp = head;
		head = temp -> next;
		free(temp -> path);
		free(temp);
	}

	memset(cmd,'\0',1024);
	sprintf(cmd, "%s", "rm ");
	strcat(cmd, path);
	system(cmd);

	fclose(fp);
	free_manifest(server_man);
	free_manifest(manifest);
	close(sock);
	return 1;
}
