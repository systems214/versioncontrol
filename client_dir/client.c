#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include"configure.h"
#include"create.h"
#include"add.h"
#include"commit.h"
#include"../helper_dir/helper.h"
#include"destroy.h"
#include"client.h"
#include"rollback.h"
#include"push.h"

int create_config(char* IP, char* port){
	int fd = open("./client_dir/.wtf/.configure", O_CREAT | O_WRONLY, 0640);
	char m[64] = {0};
	memset(m, '\0', 64);
	strcpy(m, IP);
	strcat(m, "\n");
	strcat(m, port);
	write(fd, m, strlen(m));
	close(fd);
}

int main(int argc, char** argv){

	char* project_name;
	char* IP;
	int port;

	errno = 0;
	int fd;
	//Only three arguments, the
	if(argc == 3) {
		project_name = argv[2];
		if(strcmp("checkout", argv[1]) == 0 ) {
			checkout(argv[2]);
		}
		else if(strcmp("update", argv[1]) == 0) {
			if(update(argv[2]) == -1) {
				return -1;
			}
			return 0;
		}
		else if(strcmp("upgrade", argv[1]) == 0) {
			upgrade_client(argv[2]);
		}
		else if(strcmp("commit", argv[1]) == 0) {
			return commit(argv[2]);
		}
		else if(strcmp("push", argv[1]) == 0) {
			return push(argv[2]);
		}
		else if(strcmp("create", argv[1]) == 0) {
			if(create(argv[2]) == -1){
				return -1;
			}
			return 0;
		}
		else if(strcmp("destroy", argv[1]) == 0) {
			if(destroy(argv[2]) != -1){
				return 0;
			}
			return -1;
		}
		else if(strcmp("currentversion", argv[1]) == 0) {
			return currentversion(argv[2]);
		}
		else if(strcmp("history", argv[1]) == 0) {
			return history(argv[2]);
		}
		else {
			printf("Invalid command\n");
		}
	}
	else if(argc == 4) {
		if(strcmp("configure", argv[1]) == 0)  {
			port = atoi(argv[3]);
		//	if(configure(argv[2], port) != -1){
				struct stat st = {0};
				if(stat("./client_dir/.wtf", &st) == -1){
					mkdir("./client_dir/.wtf", 0700);
				}
				create_config(argv[2], argv[3]);
		//	}
		}
		else if(strcmp("add", argv[1]) == 0) {
			add(argv[2], argv[3]);
		}
		else if(strcmp("remove", argv[1]) == 0) {
			remove_file(argv[2], argv[3]);
		}
		else if(strcmp("rollback", argv[1]) == 0) {
			
			int version = atoi(argv[3]);
			if(version < 0 ){
				printf("Invalid version\n");
				return -1;
			}
			return rollback(argv[2], version);
		}
		else {
			printf("Invalid arguements\n");
		}
	}
	else {
		printf("Invalid amount of arguements\n");
	}

	return 0;
}
