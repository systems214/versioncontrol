#include "history.h"

int history(char* project) {
	int sock;
	if(connect_client(&sock) == -1) {
		return -1;
	}

	//Send the his:project
	char buf[1024] = {0};
	sprintf(buf,"his:%s",project);
	send(sock,buf,(4+strlen(project)),0);


	//read the history file size;
	int size = 0;
	char* size_buffer[4] = {0};
	read(sock,size_buffer,4);
	size = * (int*) size_buffer;
	if(size == -1) {
		printf("Project is not found\n");
		close(sock);
		return 1;
	}
	//this has the history file
	char buffer[size];
	read(sock,buffer,size);
	fprintf(stdout,buffer);
	close(sock);
	return 1;
}
