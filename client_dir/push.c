#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include"../helper_dir/helper.h"
#include"connect_client.h"
#include"client.h"
#include"push.h"

int push(char* project){

	
	CLIENT(project);
	
	//check for .update
	char update_path[256] = {0};
	strcpy(update_path, path);
	strcat(update_path, "/.update");
	FILE* update_file = fopen(update_path, "r");

	if(update_file != NULL){
		char c = '\0';
		while(fscanf(update_file, "%c\t%*d\t%*s\t%*s\n", &c) != 0){
			printf(".update has M flagged file(s). Aborting\n");
			fclose(update_file);
			return -1;
		}
	}

	//connect to server
	int sock = 0, value;

	if(connect_client(&sock) == -1){
		return -1;
	}
	
	//send over command to server
	char command[strlen(project)+strlen("pus:")+1];
	strcpy(command, "pus:");
	strcat(command, project);
	
	value = send(sock, command, strlen(command), 0);
	
	//receive confirmation from server for connection
	char confirm[strlen("connected.")+1];
	memset(confirm, '\0', strlen("connectd")+1);
	value = recv(sock, confirm, strlen("connected"), 0);
		
	//find commit
	DIR* dir = opendir(path);
	struct dirent* dirent = NULL;
	char commit_path[256] = {0};
	char commit_name[20] = {0};

	while((dirent = readdir(dir)) != NULL){
		if(strncmp(dirent->d_name, ".commit", 7) == 0){
			sprintf(commit_path, "%s/%s", path, dirent->d_name);
			sprintf(commit_name, "%s", dirent->d_name);
			break;
		}
	}
	closedir(dir);
	
	printf("%s\n", commit_path);
	//confirm commit existence
	FILE* commit_file = fopen(commit_path, "r");
	if(commit_file == NULL){
		printf("Local commit file DNE. Run commit before push\n");
		return -1;
	}
	int commit_name_len = strlen(commit_name);

	//send commit name
	value = send(sock, (char*)&commit_name_len, sizeof(int), 0);
	value = send(sock, commit_name, commit_name_len, 0);
	
	value = recv(sock, confirm, strlen("commit DNE"), 0);
	if(strcmp(confirm, "commit DNE") == 0){
		printf("server commit DNE. Aborting.\n");
		char delete[256] = {0};
		strcpy(delete, "rm ");
		strcat(delete, commit_path);
		system(delete);
		printf("Commit again before push\n");
		return -1;
	}


	//make and send commit stream
	char* commit_stream = NULL;
	char commit_buffer[256] = {0};
	int stream_len = 0;
	
	int num_files = 0;
	int version = 0;

	fscanf(commit_file, "%d %d\n", &num_files, &version);
	
	//files to send to server
	char* to_send[num_files];
	int version_flag = 0;
	char commit_f[256] = {0};	
	int i=0;

	while(fgets(commit_buffer, 256, commit_file) != NULL){
		sscanf(commit_buffer, "%d\t%*s\t%s\n", &version_flag, commit_f);
		if(version_flag != -1){
			to_send[i] = (char*) malloc(sizeof(char)*256);
			sprintf(to_send[i++], "%s", commit_f);
		}
		char * temp_stream = NULL;
		temp_stream = commit_stream;
		commit_stream = (char*) malloc(sizeof(char)*(stream_len+strlen(commit_buffer)+1));
		if(temp_stream != NULL){
			printf("%s\n", temp_stream);
			sprintf(commit_stream, "%s%s", temp_stream, commit_buffer);
			stream_len = strlen(commit_stream);
			free(temp_stream);
		}else{
			sprintf(commit_stream+stream_len, "%s", commit_buffer);
			stream_len = strlen(commit_stream);
		}

	}
	fclose(commit_file);

	printf("%s\n", commit_stream);
	send(sock, (char*)&num_files, sizeof(int), 0);
	send(sock, (char*)&version, sizeof(int), 0);
	send(sock, (char*)&stream_len, sizeof(int), 0);
	send(sock, commit_stream, stream_len, 0);
	
	char cmd[256] = {0};
	sprintf(cmd, "rm %s", commit_path);

	//confirm commit match
	char reply[20] = {0};
	recv(sock, reply, strlen("not match")+1, 0);
	if(strcmp(reply, "not match") == 0){
		printf("No matching commit on server. Commit may have expired.\n");
		printf("Removing commit\n");
		system(cmd);
		return -1;
	}
	
	free(commit_stream);

	char mkdir[256] = {0};
	sprintf(mkdir, "mkdir %s/package", path);
	system(mkdir);

	int j;
	for(j=0; j<i; j++){
		char cp[1024] = {0};
		struct stat st = {0};
		char st_path[1024] = {0};
		sprintf(st_path, "%s/%s", path, to_send[j]);
		stat(st_path, &st);
		
		if((st.st_mode & S_IFREG)){
			sprintf(cp, "cp %s/%s %s/package", path, to_send[j], path);
		}else{
			sprintf(cp, "cp -R %s/%s %s/package", path, to_send[j], path);
		}
		system(cp);
	}


	struct manifest * manifest =  (struct manifest*) malloc(sizeof(struct manifest));
	manifest = load_manifest(1,project);
	char manifest_path[1024] = {0};
	sprintf(manifest_path,"client_dir/%s/.manifest",project);
	remove(manifest_path);

	FILE* manifest_file = fopen(manifest_path,"w+");
	manifest -> high_ver = manifest -> high_ver + 1;
	fprintf(manifest_file,"%d %d\n",manifest -> size, manifest -> high_ver);
	for(i = 0; i < manifest -> size; i++) {
		struct entry * temp_entry = manifest -> man_list[i];
		if(temp_entry == NULL) {
			continue;
		}
		fprintf(manifest_file,"%d\t%s\t%s\n",temp_entry ->  version, temp_entry -> hash, temp_entry -> path);
	}
	free_manifest(manifest);
	manifest = NULL;
	fclose(manifest_file);
	
	char cp[1024] = {0};
	sprintf(cp, "cp %s/.manifest %s/package", path, path);
	system(cp);

	char tar[1024] = {0};
	sprintf(tar, "tar -czvf %s/package.tar.gz -C %s package", path, path);
	system(tar);

	char package_path[1024] = {0};
	sprintf(package_path, "%s/package.tar.gz", path);
	FILE* package = fopen(package_path, "rb");

	fseek(package, 0L, SEEK_END);
	int package_len = ftell(package);
	fseek(package, 0L, SEEK_SET);

	send(sock, (char*)&package_len, sizeof(int), 0);
	char buffer[package_len+1];
	memset(buffer, '\0', package_len+1);

	fread(buffer, package_len, sizeof(char), package);
	fclose(package);
	send(sock, buffer, package_len, 0);
	
	sleep(3);
	sprintf(tar, "rm -rf %s/package.tar.gz %s/package", path, path);
	system(tar);

	for(j=0; j<i; j++){
		free(to_send[j]);
	}
	printf("Removing local .commit\n");
	system(cmd);
	return 0;
}

