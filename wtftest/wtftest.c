#include<stdlib.h>
#include<stdio.h>
#include<errno.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>

int client_fork(char* command, char* project) {
	pid_t pid = fork();
	if(pid == -1) {
		return -1;
	}
	else if (pid > 0) {
		return pid;
	}
	else {
		char* args[] = {"./WTF", command, project, NULL};
		execv(args[0],args);
	}
	return -1;
}

int server_fork(char* port) {
	pid_t pid = fork();
	if(pid == -1) {
		return -1;
	}
	else if (pid > 0) {
		return pid;
	}
	else {
		char* args[] = {"./WTFserver", port, NULL};
		execv(args[0],args);
	}
	return -1;
}

int client_fork2(char* command, char* project, char* command2) {
	pid_t pid = fork();
	if(pid == -1) {
		return -1;
	}
	else if (pid > 0) {
		return pid;
	}
	else {
		char* args[] = {"./WTF", command, project,command2, NULL};
		execv(args[0],args);
	}
	return -1;
}


int main (int argc, char ** argv) {
	int status;


	printf("Test 1\n");

	printf("Destroying Test1 to start tests\n");
	pid_t client_10 = client_fork("destroy", "Test1");
	system("rm -rf client_dir/Test1");
	sleep(2);

	printf("Creating Test1 Project\n");
	pid_t client_1 = client_fork("create", "Test1");

	sleep(1);

	printf("Test client will now populate directory, Test1 with files\n");
	system("touch client_dir/Test1/file2.txt");
	system("touch client_dir/Test1/file1.txt");
	system("touch client_dir/Test1/file3.txt");

	sleep(1);

	system("echo 'file1' > client_dir/Test1/file1.txt");
	system("echo 'file2' > client_dir/Test1/file2.txt");
	system("echo 'file3' > client_dir/Test1/file3.txt");


	printf("Adding file, file1.txt\n");
	pid_t client_2 = client_fork2("add", "Test1", "file1.txt");
	sleep(1);
	printf("Adding file, file2.txt\n");

	pid_t client_3 = client_fork2("add", "Test1", "file2.txt");
	sleep(1);
	printf("Commiting Test1\n:");

	pid_t client_4 = client_fork("commit", "Test1");
	sleep(2);
	printf("Pushing test1\n");

	pid_t client_5 = client_fork("push", "Test1");

	sleep(2);

	printf("Removing file2.txt\n");
	pid_t client_6 = client_fork2("remove", "Test1", "file2.txt");

	sleep(1);

	printf("adding in file 3\n");
	pid_t client_7 = client_fork2("add", "Test1", "file3.txt");


	printf("modifying file1.txt\n");

	system("echo 'file1file1file1' > client_dir/Test1/file1.txt");

	
	sleep(2);

	printf("committing the changes again\n");
	pid_t client_8 = client_fork("commit", "Test1");

	sleep(3);

	printf("Pushing the changes\n");
	pid_t client_9 = client_fork("push", "Test1");

	sleep(2);

	printf("Rolling back to previous verison\n");
	pid_t client_18 = client_fork2("rollback", "Test1", "2");

	sleep(1);

	printf("asking for history\n");
	pid_t client_0 = client_fork("history", "Test1");

	sleep(1);

	printf("Test 1 is now complete\n");

	printf("\n\n");
	
	sleep(2);

	printf("Test2\n");

	printf("Creating necssary files to perform Test2\n");
	system("rm -rf client_dir/UpdateUp");
	system("cp -R UpdateUp client_dir/");
	sleep(3);

	printf("Performing update\n");
	pid_t client_11 = client_fork("update", "UpdateUp");

	sleep(2);
	printf("Performing upgrade\n");
	pid_t client_12 = client_fork("upgrade", "UpdateUp");

	sleep(2);
	printf("Performing update to verify on same version as manifest\n");
	pid_t client_13 = client_fork("update", "UpdateUp");

	printf("Test2 is complete\n");


	printf("Test3\n");

	sleep(2);
	printf("Performing multple requests,update, history, commit, and create\n");

	pid_t client_14 = client_fork("update", "UpdateUp");
	pid_t client_15 = client_fork("history", "Test1");
	pid_t client_16 = client_fork("update", "Test1");
	pid_t client_17 = client_fork("create", "Test2");

	printf("Test3 is complete\n");



	sleep(2);




	if(client_0 > 0) {
		kill(client_1, SIGINT) == 0;
	}

	if(client_1 > 0) {
		kill(client_1, SIGINT) == 0;
	}

	if(client_2 > 0) {
		kill(client_2, SIGINT) == 0;
	}
	if(client_3 > 0) {
		kill(client_3, SIGINT) == 0;
	}

	if(client_4 > 0) {
		kill(client_4, SIGINT) == 0;
	}

	if(client_5 > 0) {
		kill(client_5, SIGINT) == 0;
	}
	if(client_6 > 0) {
		kill(client_6, SIGINT) == 0;
	}

	if(client_7 > 0) {
		kill(client_7, SIGINT) == 0;
	}

	if(client_8 > 0) {
		kill(client_8, SIGINT) == 0;
	}
	if(client_9 > 0) {
		kill(client_9, SIGINT) == 0;
	}
	if(client_10 > 0) {
		kill(client_10, SIGINT) == 0;
	}
	if(client_11 > 0) {
		kill(client_11, SIGINT) == 0;
	}
	if(client_12 > 0) {
		kill(client_12, SIGINT) == 0;
	}
	if(client_13 > 0) {
		kill(client_13, SIGINT) == 0;
	}
	if(client_14 > 0) {
		kill(client_14, SIGINT) == 0;
	}
	if(client_15 > 0) {
		kill(client_15, SIGINT) == 0;
	}
	if(client_16 > 0) {
		kill(client_16, SIGINT) == 0;
	}
	if(client_17 > 0) {
		kill(client_17, SIGINT) == 0;
	}
	if(client_18 > 0) {
		kill(client_18, SIGINT) == 0;
	}

	printf("All child proccesses have been terminated\n");
	return 1;
}
