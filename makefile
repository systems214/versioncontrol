all:
	$(MAKE) -C server_dir
	$(MAKE) -C client_dir

clean:
	rm WTF WTFserver WTFtest

test:
		$(MAKE) -C wtftest
		$(MAKE) -C server_dir
		$(MAKE) -C client_dir
