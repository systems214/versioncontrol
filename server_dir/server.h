#ifndef _server_h
#define _server_h
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <dirent.h>
#include <pthread.h>
#include "../helper_dir/helper.h"
#include "server_history.h"
#include "server_checkout.h"

#define SERVER(x)\
	char path[1024];\
	memset(path, '\0', 1024);\
	strcat(path, "server_dir/");\
	strcat(path, x);\

static int** socket_array;
static pthread_mutex_t table_lock;
static pthread_mutex_t* project_locks;
static pthread_mutex_t thread_counter_lock;
static char** project_paths; //project names on server
static int num_projects;	//number of projects
static int table_access;
static int thread_counter;

struct args{
	int sock;
	char* buffer;
	pthread_t tid;
};

#endif
