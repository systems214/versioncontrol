#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/types.h>
#include<errno.h>
#include<unistd.h>
#include<pthread.h>
#include<dirent.h>
#include"../helper_dir/helper.h"
#include"server.h"
#include"server_upgrade.h"
#include<dirent.h>

int upgrade_project(int new_socket,char* project, pthread_mutex_t * mtx){


	char confirm;

	SERVER(project);
	strcat(path,"/.manifest");
	FILE* check = NULL;
	check = fopen(path,"r");
	if(check == NULL) {
		printf("Project is not found\n");
		send(new_socket, "0", 1, 0);
		return -1;
	}
	else {
		printf("Project is found\n");
		send(new_socket, "1", 1, 0);
	}

	//Incrementing thread counter saying im working on a project
	pthread_mutex_lock(&table_lock);



	int size = 0;
	char buffer[4];
	memset(buffer,'\0', 4);
	read(new_socket, buffer, sizeof(int));
	size = * (int *) buffer;


	confirm = '1';
	send(new_socket, &confirm, 1, 0);
	//make the directory
	char cmd[1024];
	memset(cmd, '\0', 1024);
	sprintf(cmd, "mkdir server_dir/%s/package",project);
	system(cmd);
	//size is the amount files we are going to be receiving;

	int i = 0;
	for(i = 0; i < size; i++) {
		DIR* dir = NULL;
		char filename[1024];
		memset(filename, '\0', 1024);
		int filename_size = 0;
		read(new_socket, buffer, sizeof(int));
		filename_size = * (int*) buffer;

		char name[1024];
		memset(name, '\0', 1024);
		read(new_socket, name, filename_size);
		char temp[1024];
		memset(temp, '\0', 1024);
		char * temp_ptr = temp;
		temp_ptr = strchr(name,'/');
		char cmd[1024];
		memset(cmd,'\0',1024);
		if(temp_ptr != NULL) {
			strncpy(temp,name,temp_ptr-name);
			sprintf(cmd,"cp -R server_dir/%s/%s server_dir/%s/package", project, temp, project); //if the file is a directory, then i use -R
		}
		else {
			sprintf(cmd,"cp server_dir/%s/%s server_dir/%s/package", project, name, project);
		}
		system(cmd);
	}

	send(new_socket, &confirm, 1, 0);

	//Now we compress the file
	memset(cmd, '\0', 1024);
	sprintf(cmd, "tar -czvf server_dir/%s/package.tar.gz -C server_dir/%s package",project, project); //compress the file and place it in the server_dir/project, calling it package.tar.gz, then go into the server_dir/project and compress the package directory (you need the -C, otherwise it gets all fucked up)


	system(cmd);
	memset(cmd, '\0' ,1024);
	sprintf(cmd, "server_dir/%s/package.tar.gz", project);
	FILE* fp = fopen(cmd, "r");
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	//send file size
	send(new_socket, (char*)&size, sizeof(int),0);
	//send actual file
	fclose(fp);
	read(new_socket, &confirm, 1);

	memset(cmd, '\0', 1024);
	sprintf(cmd, "server_dir/%s/package.tar.gz", project);
	FILE* fd = fopen(cmd, "r"); //use fopen to open the packaged file, otherwise it gets all fucked
	char crpt[size];
	int value = -1;
	value = fread(crpt,sizeof(char),size,fd);
	send(new_socket,crpt,size,0);
	fclose(fd);
	memset(cmd, '\0', 1024);
	sprintf(cmd, "rm server_dir/%s/package.tar.gz", project);
	system(cmd);
	memset(cmd, '\0', 1024);
	sprintf(cmd, "rm -rf server_dir/%s/package", project);
	system(cmd);
	//sent file


	value = -1;
	FILE* fp2 = fopen(path, "r");
	fseek(fp2, 0L, SEEK_END);
	int man_size = ftell(fp2);
	rewind(fp2);
	char man_buffer[man_size];
	value = fread(man_buffer,man_size, 1, fp2);
	send(new_socket,(char*)&man_size,sizeof(int),0);

	read(new_socket,&confirm,1);


	send(new_socket,man_buffer,man_size,0);
	fclose(fp2);

	printf("Upgrade is Done\n");
	pthread_mutex_lock(&table_lock);
	close(new_socket);
	return 1;
}
