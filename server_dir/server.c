#include"server.h"

int server_fd = -1;


void sigint_handler(){

		printf("\nShutting down\n");
		if(server_fd > 0){
			close(server_fd);
		}
		int i;
		for(i=0; i<num_projects; i++){
			pthread_mutex_destroy(&project_locks[i]);
		}
		pthread_mutex_destroy(&table_lock);
		pthread_mutex_destroy(&thread_counter_lock);
		for(i=0; i<num_projects; i++){
			free(project_paths[i]);
		}
		for(i = 0; i < 100; i++) {
			if(socket_array[i] != NULL) {
				free(socket_array[i]);
				socket_array[i] = NULL;
			}
		}
		free(project_paths);
		free(project_locks);
		exit(1);
}

pthread_mutex_t* get_project_mutex(char* project){

	int i;
	for(i=0; i<num_projects; i++){
		if(strcmp(project, project_paths[i]) == 0){
			return &(project_locks[i]);
		}
	}
	return NULL;
}


void* thread_client(void* sock){

	//chars read
	int valread;

	int new_socket = *((int*) sock);

	//buffer to write read data
	char buffer[1024] = {0};

	//send msg to client to confirm it is ready to receive msgs

	//read client sent data
	valread = read(new_socket, buffer, 1024);

	printf("Received message - %s - from client\n", buffer);

	if(valread <= 0){
		printf("Nothing read from client\n");
	}else{


		char cmd[256];
		char project[strlen(buffer)];
		memset(project, '\0', strlen(buffer));
		strncpy(project, buffer+4, strlen(buffer)-4);

		if(strncmp(buffer, "cre:", 4) == 0){

			pthread_mutex_lock(&table_lock);

			while(thread_counter != 0){};

			printf("Attempting to create project...\n");

			sprintf(cmd, "%s", "mkdir ");

			strcat(cmd, "server_dir/");
			strcat(cmd, project);

			if(system(cmd) != -1){
				send(new_socket, "Project created", strlen("Project created"), 0);
				printf("%d\n", new_socket);
				printf("Project created\n");

				SERVER(project);
				char path2[1024] = {0};
				sprintf(path2, "%s", path);
				strcat(path, "/.manifest");
				
				printf("%s\n", path);

				FILE* manifest = fopen(path, "w+");
				fwrite((void*) "0 0\n", 1, strlen("0 0\n"), manifest);
				fclose(manifest);

				sprintf(cmd, "mkdir %s/history", path2);
				system(cmd);

				sprintf(cmd, "touch %s/history/.history", path2);
				system(cmd);

				sprintf(cmd, "echo \"create\n0\" >> %s/history/.history", path2);
				system(cmd);

				char** temp = project_paths;
				project_paths = (char**) malloc(sizeof(char*)*(num_projects+1));
				int i;
				for(i=0; i < num_projects; i++){
					project_paths[i] = (char*) malloc(sizeof(char)*(strlen(temp[i])+1));
					project_paths[i] = temp[i];
				}
				free(temp);

				project_paths[num_projects] = (char*) malloc(sizeof(char)*(strlen(project)+1));
				strcpy(project_paths[num_projects], project);

				pthread_mutex_t* temp_m = project_locks;
				project_locks = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t)*++num_projects);

				//mutexes for projects
				for(i=0; i<num_projects; i++){
					project_locks[i] = temp_m[i];
				}
				free(temp_m);
				pthread_mutex_init(&project_locks[num_projects-1], NULL);

			}else{
				send(new_socket, "Project exists", strlen("Project exists"), 0);
				printf("Project Exists\n");

			}

			pthread_mutex_unlock(&table_lock);

		}else if(strncmp(buffer, "des:", 4)==0){


			pthread_mutex_lock(&table_lock);

			while(thread_counter != 0){}

			printf("Attempting to destroy project...\n");

			sprintf(cmd, "%s", "rm -rf ");
			strcat(cmd, "server_dir/");
			strcat(cmd, project);

			if(system(cmd) != -1){

				send(new_socket, "Project Destroyed", strlen("Project Destroyed"), 0);
				printf("Project Destroyed\n");
			}else{
				send(new_socket, "Project DNE", strlen("Project DNE"), 0);
				printf("Project DNE\n");
			}


			pthread_mutex_unlock(&table_lock);

		}else if(strncmp(buffer, "upd:", 4) == 0) {



			pthread_mutex_lock(&table_lock);


				SERVER(project);
				strcat(path,"/.manifest");
				FILE* fp = fopen(path, "r");
				int size = -1;
				if(fp == NULL) {
					printf("Project does not exists, unable to locate manifest file\n");
					send(new_socket, (char *) &size, sizeof(int), 0);
				}
				else {
					fseek(fp, 0L, SEEK_END); //set cursor to end of the file
					size = ftell(fp); //grab the number of bytes in the file
					send(new_socket, (char*)&size,sizeof(int),0); //send the size of the file

					fclose(fp);
					memset(path,'\0',1024);
					strcat(path, "server_dir/");
					strcat(path,project);
					strcat(path,"/.manifest");

					int fd = open(path,0640);
					printf("%s\n",path);
					printf("%d\n",fd);
					//send the contents of the manifest
					sendfile(new_socket, fd, NULL, size);
					close(fd);
					printf("Completed update\n");
				}
		pthread_mutex_unlock(&table_lock);

		}else if(strncmp(buffer, "pus:", 4) == 0){
			pthread_mutex_lock(&table_lock);

			SERVER(project);
			send(new_socket, "connected.", strlen("connected."), 0);

			int commit_name_len = 0;
			valread = recv(new_socket, (char*)&commit_name_len, sizeof(int), 0);

			char commit_name[commit_name_len + 1];
			memset(commit_name, '\0', commit_name_len+1);
			valread = recv(new_socket, commit_name, commit_name_len, 0);

			char commit_path[strlen(path)+commit_name_len+1];
			sprintf(commit_path, "%s/%s", path, commit_name);

			DIR* dir = opendir(path);
			struct dirent* dirent;

			int found = 0;
			while((dirent = readdir(dir)) != NULL){
				if(strcmp(dirent->d_name, commit_name) == 0){
					found = 1;
					break;
				}
			}
			closedir(dir);

			if(found == 1){
				send(new_socket, "has commit", strlen("has commit"), 0);

				int num_client = 0;
				recv(new_socket, (char*)&num_client, sizeof(int), 0);

				int client_version = 0;
				recv(new_socket, (char*)&client_version, sizeof(int), 0);

				int commit_stream_len = 0;
				recv(new_socket, (char*)&commit_stream_len, sizeof(int), 0);

				char commit_stream[commit_stream_len+1];
				memset(commit_stream, '\0', commit_stream_len+1);

				recv(new_socket, commit_stream, commit_stream_len, 0);

				FILE* server_commit = fopen(commit_path, "r");

				int num_server = 0;
				int server_version = 0;

				fscanf(server_commit, "%d %d\n", &num_server, &server_version);

				if(num_server == num_client && server_version == client_version){
					int curr_position = ftell(server_commit);
					fseek(server_commit, 0L, SEEK_END);
					int server_commit_len = ftell(server_commit) - curr_position;
					fseek(server_commit, 0L, SEEK_SET);

					fscanf(server_commit, "%*d %*d\n");
					char server_buffer[server_commit_len+1];
					memset(server_buffer, '\0', server_commit_len+1);

					fread(server_buffer, server_commit_len, sizeof(char), server_commit);

					if(strcmp(server_buffer, commit_stream) == 0){

						send(new_socket, "yes match\0", strlen("not match")+1, 0);

						fclose(server_commit);

						printf("Expiring pending commits\n");

						char syscall[1024] = {0};
						sprintf(syscall, "rm %s/.commit*", path);
						system(syscall);

						sprintf(syscall, "mkdir %s/%d", path, server_version);
						system(syscall);

						dir = opendir(path);
						while((dirent = readdir(dir)) != NULL){
							if(strcmp(dirent->d_name, ".") == 0 || strcmp(dirent->d_name, "..") == 0 || strcmp(dirent->d_name, "history") == 0 || (atoi(dirent->d_name) == server_version) ) continue;

							if(dirent->d_type == DT_DIR){
								sprintf(syscall, "cp -R %s/%s %s/%d", path, dirent->d_name, path, server_version);
							}else{
								sprintf(syscall, "cp %s/%s %s/%d", path, dirent->d_name, path, server_version);

							}
							system(syscall);
						}

						sprintf(syscall, "tar -czvf %s/%d.tar.gz -C %s %d", path, server_version, path, server_version);
						system(syscall);

						sprintf(syscall, "mv %s/%d.tar.gz %s/history", path, server_version, path);
						system(syscall);

						sprintf(syscall, "rm -rf %s/%d", path, server_version);
						system(syscall);

						int package_len = 0;
						recv(new_socket, (char*)&package_len, sizeof(int), 0);

						char package_buf[package_len+1];
						memset(package_buf, '\0', package_len+1);
						recv(new_socket, package_buf, package_len, 0);

						char package_path[strlen(path)+strlen("package.tar.gz")+1];
						sprintf(package_path, "%s/package.tar.gz", path);
						FILE* package = fopen(package_path, "w+");

						fwrite(package_buf, sizeof(char), package_len, package);
						fclose(package);

						sprintf(syscall, "tar -C %s -zxvf %s/package.tar.gz", path, path);
						system(syscall);

					//*
						sprintf(syscall, "cp -R %s/package/* %s", path, path);
						system(syscall);
		
						sprintf(syscall, "cp %s/package/.manifest %s", path, path);
						system(syscall);
						//*
						sprintf(syscall, "rm -rf %s/package", path);
						system(syscall);

						sprintf(syscall, "rm %s/package.tar.gz", path);
						system(syscall);

						char history_buf[1024] = {0};
						int ver = 0;
						char hash[41] = {0};
						char pathing[256] = {0};
						int bytes = 0;
						int z = 0;

						char history_path[strlen(path) + strlen("/history/.history") + 1];
						memset(history_path, '\0', strlen(path) + strlen("/history/.history") +1);
						sprintf(history_path, "%s/history/.history", path);
						FILE* history_file = fopen(history_path, "a");

						fprintf(history_file, "\npush\n%d\n", server_version);
					
						int zk = 0;
						while(zk<num_client){
							sscanf(commit_stream+z, "%d\t%s\t%s\n%n", &ver, hash, pathing, &bytes);
							if(ver == -1){
								sprintf(syscall, "rm %s/%s", path, pathing);
								system(syscall);
								fprintf(history_file, "D\t%d\t%s\t%s\n", ver, hash, pathing);
							}else if(ver == 0){
								fprintf(history_file, "A\t%d\t%s\t%s\n", ver, hash, pathing);
							}else{
								fprintf(history_file, "M\t%d\t%s\t%s\n", ver, hash, pathing);
							}
							zk++;
							z = bytes + z;
						}

						fclose(history_file);
						//*/
						printf("Push was successful\n");

					}else{
						send(new_socket, "not match", strlen("not match"), 0);
						printf("commit files do not match\n");
						fclose(server_commit);
					}
				}else{
					send(new_socket, "not match", strlen("not match"), 0);
					printf("commit versions do not match\n");
					fclose(server_commit);
				}
			}else{
				printf("commit file not found\n");
				send(new_socket, "not match", strlen("not match"), 0);
			}

			printf("Push has completed\n");
			pthread_mutex_unlock(&table_lock);


		}else if(strncmp(buffer, "com:", 4) == 0){
			pthread_mutex_lock(&table_lock);


			SERVER(project);
			char man_path[256];
			strcpy(man_path, path);
			strcat(man_path, "/.manifest");

			FILE* server_manifest = fopen(man_path, "r");
			if(server_manifest == NULL){
				printf("Project DNE\n");
				int x = -1;
				send(new_socket, (char*)&x, sizeof(int), 0);
			}else{


				int num_files = 0;
				int version = 0;
				fscanf(server_manifest, "%d %d\n", &num_files, &version);

				send(new_socket, (char*)&version, sizeof(int), 0);

				char reply[20] = {0};
				valread = recv(new_socket, reply, 20, 0);

				if(strcmp(reply, "version diff") == 0){
					printf("%s", reply);
					fclose(server_manifest);

				}else{
					printf("%s\n", reply);
					send(new_socket, (char*)&num_files, sizeof(int), 0);

					int file_version = 0;
					char hash[41] = {0};
					char file_path[256] = {0};
					char message[1024] = {0};
					int i=0;
					while(fgets(message, 1024, server_manifest) != NULL){

						int message_len = strlen(message);
						send(new_socket, (char*)&message_len, sizeof(int), 0);
						send(new_socket, message, message_len, 0);
					}
					fclose(server_manifest);

					valread = recv(new_socket, reply, strlen("no change")+1, 0);
					if(strcmp(reply, "no change") == 0){
						printf("%s", reply);
					}else{
						DIR* dir = opendir(path);
						struct dirent* dirent;
						int client_num = 0;
						while((dirent = readdir(dir))!=NULL){
							if(strncmp(dirent->d_name, ".commit", 7) == 0){
								client_num = atoi(dirent->d_name+7)+1;
								//printf("%d\n", dirent->d_name+7);
							}
						}
						send(new_socket, (char*)&client_num, sizeof(int), 0);
						closedir(dir);
						//printf("client_num: %d\n", client_num);

						int num_files = 0;
						valread = recv(new_socket, (char*)&num_files, sizeof(int), 0);
						int version = 0;
						valread = recv(new_socket, (char*)&version, sizeof(int), 0);

						int len = 0;
						valread = recv(new_socket, (char*)&len, sizeof(int), 0);
						char commit_buf[len+1];
						memset(commit_buf, '\0', len+1);
						valread = recv(new_socket, commit_buf, len+1, 0);

						printf("Receiving .commit\n");
						char commit_path[256] = {0};
						sprintf(commit_path, "%s%s%d", path, "/.commit", client_num);
						//printf("%s\n", commit_path);

						FILE* commit_file = fopen(commit_path, "w+");
						if(strlen(commit_buf) == 0 || commit_file == NULL){
							printf("commit not received from client or .commit file not create locally. Aborting\n");
							send(new_socket, "not receive\0", strlen("not receive")+1, 0);
						}else{
							fprintf(commit_file, "%d %d\n", num_files, version);
							fprintf(commit_file, "%s", commit_buf);
							fclose(commit_file);
							send(new_socket, "yes receive\0", strlen("yes receive")+1, 0);
						}
					}
				}
			}
			printf("Commit has completed\n");
			pthread_mutex_unlock(&table_lock);

		}else if(strncmp(buffer, "cur:", 4)==0){

			pthread_mutex_lock(&table_lock);

			SERVER(project);
			strcat(path, "/.manifest");

			FILE* manifest = fopen(path, "r");
			if(manifest == NULL){

				printf("Project DNE\n");
				int x = -1;
				send(new_socket, (char*)&x, sizeof(int), 0);

			}else{

				printf("Fetching current version of %s\n", project);
				int num_files = 0;
				int project_version = 0;
				fscanf(manifest, "%d %d\n", &num_files, &project_version);

				send(new_socket, (char*)&project_version, sizeof(int), 0);

				int i;
				int version = 0;
				char file_path[1024] = {0};

				int message_len = 0;
				char* message = NULL;
				for(i=0; i < num_files; i++){
					if(fscanf(manifest, "%d\t%*s\t%s\n", &version, file_path) == EOF) break;
					char* temp = message;
					message = (char*) malloc(sizeof(char)*(message_len+strlen(file_path)+5));
					memset(message, '\0', message_len+strlen(file_path));

					if(temp != NULL){
						strcpy(message, temp);
						free(temp);
					}
					sprintf(message+message_len, "%d\t", version);
					sprintf(message+strlen(message), "%s\n", file_path);
					message_len = strlen(message);
				}
				send(new_socket, (char*)&message_len, sizeof(int), 0);
				send(new_socket, message, message_len, 0);

				fclose(manifest);
					if(message != NULL){
					free(message);
				}
				}
			printf("Current Version complete\n");
			pthread_mutex_unlock(&table_lock);

		}else if(strncmp(buffer, "upg:", 4)==0){
			pthread_mutex_t* mtx = get_project_mutex(project);
			upgrade_project(new_socket, project, mtx);
		}else if(strncmp(buffer, "his:",4) == 0) {
			pthread_mutex_t* mtx = get_project_mutex(project);
			server_history(new_socket,project,mtx);
		}else if(strncmp(buffer, "chk:",4) == 0) {
			pthread_mutex_t* mtx = get_project_mutex(project);
			server_checkout(new_socket,project,mtx);
		}else if(strncmp(buffer, "rol:", 4) == 0){
			
			pthread_mutex_lock(&table_lock);

			while(thread_counter != 0){};
			
			send(new_socket, "rec", 3, 0);

			int version = 0;
			recv(new_socket, (char*)&version, sizeof(int), 0);

			SERVER(project);

			char history_path[strlen(path) + strlen("/history/.history") + 1];
			memset(history_path, '\0', strlen(path) + strlen("/history/.history") + 1);
			sprintf(history_path, "%s/history/.history", path);

			FILE* history_file = fopen(history_path, "a");

			fprintf(history_file, "\nrollback\n%d", version);
			fclose(history_file);

			char history_dir[strlen(path) + strlen("/history") + 1];
			sprintf(history_dir, "%s/history", path);
			
			DIR* dir1 = opendir(path);
			struct dirent* dirent;

			while((dirent = readdir(dir1)) != NULL){
				if(strcmp(dirent->d_name, "history") == 0 || strcmp(dirent->d_name, ".") == 0 || strcmp(dirent->d_name, "..") == 0) continue;

				char file[1024] = {0};
				sprintf(file, "rm -rf %s/%s", path, dirent->d_name);
				system(file);
			}
			closedir(dir1);

			DIR* dir = opendir(history_dir);

			char vertar[strlen(".tar.gz") + strlen((char*)&version) + 1];
			sprintf(vertar, "%d.tar.gz", version);

			while((dirent = readdir(dir)) != NULL){
				if(strcmp(dirent->d_name, vertar) == 0){
					char syscall[1024] = {0};
					
					sprintf(syscall, "tar -C %s -zxvf %s/history/%s", path, path, vertar);
					system(syscall);
					
					sprintf(syscall, "cp -R %s/%d/* %s", path, version, path);
					system(syscall);
		
					sprintf(syscall, "cp %s/%d/.manifest %s", path, version, path);
					system(syscall);
						
					sprintf(syscall, "rm -rf %s/%d", path, version);
					system(syscall);

					sprintf(syscall, "rm %s/%s", path, vertar);
					system(syscall);
				}else if(atoi(dirent->d_name) > atoi(vertar)){
					
					char syscall[1024] = {0};
					sprintf(syscall, "rm %s/history/%s", path, dirent->d_name);
					system(syscall);
				}
			}

			send(new_socket, "suc", 3, 0);
			printf("Project rolled back to %d", version);
			closedir(dir);
			pthread_mutex_unlock(&table_lock);
		}
	}
}

int main(int argc, char const *argv[]) {

	//signal listener for ctrl+c
	signal(SIGINT, sigint_handler);

	//server and client socket
	socket_array = (int**) malloc(sizeof(int*)*100);
	int* new_socket = (int*) malloc(sizeof(int));
	int i = 0;
	for(i = 0; i < 100; i++) {
		socket_array[i] = NULL;
	}

	//socket address
	struct sockaddr_in address;

	//optional. options set for socket.
	int opt = 1;

	//get socket
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	//set socket options. Reusable address and port set.
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//idk
	address.sin_family = AF_INET;
	//socket ip address. probably 127.0.0.1
	address.sin_addr.s_addr = INADDR_ANY;
	//socket port. defined in terminal input
	address.sin_port = htons(atoi(argv[1]));

	//bind socket to address
	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	//set as passive socket. set max queue to 10 request because why not
	if (listen(server_fd, 10) < 0){
		perror("listen");
		exit(EXIT_FAILURE);
	}

	//address length
	int addrlen = sizeof(sizeof(struct sockaddr_in));

	//projects on server
	DIR* dir = opendir("./server_dir");
	project_paths = NULL;
	num_projects = 0;
	struct dirent* dirent;
	while((dirent = readdir(dir)) != NULL){
		if(dirent->d_type != DT_DIR) continue;
		if((strcmp(dirent->d_name, ".") == 0) || (strcmp(dirent->d_name, "..") == 0)) continue;

		char** temp_paths = project_paths;

		project_paths = (char**) malloc(sizeof(char*)*(++num_projects));
		project_paths[num_projects-1] = (char*) malloc(sizeof(char)*(strlen(dirent->d_name)+1));
		memset(project_paths[num_projects-1], '\0', strlen(dirent->d_name)+1);
		strcat(project_paths[num_projects-1], dirent->d_name);

		if(temp_paths != NULL){
			for(i=0; i<num_projects-1; i++){
				project_paths[i] = temp_paths[i];
			}
		}
		free(temp_paths);
	}
	closedir(dir);

	project_locks = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t)*num_projects);
	//mutexes for projects
	for(i=0; i<num_projects; i++){
		pthread_mutex_init(&(project_locks[i]), NULL);
	}

	pthread_mutex_init(&table_lock, NULL);
	pthread_mutex_init(&thread_counter_lock, NULL);

	thread_counter = 0;
	table_access = 1;
	//define client socket when client sends configuration
	int socket_index = 0;
	socket_array[socket_index] = new_socket;
	socket_index++;
	while(1){
		if ((*new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0){
			perror("accept");
			exit(EXIT_FAILURE);
		}
		printf("ACCEPTING NEW SOCKET\n");

		pthread_t tid;
		pthread_create(&(tid), NULL, thread_client, (void*)new_socket);
		pthread_detach(tid);
		new_socket = (int *) malloc(sizeof(int));
		socket_array[socket_index];
		socket_index++;
	}


	return 0;
}
