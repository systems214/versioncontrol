#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/types.h>
#include<errno.h>
#include<unistd.h>
#include<pthread.h>
#include<dirent.h>
#include"../helper_dir/helper.h"
#include"server.h"
#include"server_history.h"

int server_history(int socket,char* project, pthread_mutex_t * mtx){
	pthread_mutex_lock(&table_lock);



	int size = -1;
	if(project == NULL) {
		send(socket,(char*)&size,sizeof(int),0);
		printf("Project was not found\n");

		return -1;
	}
	char path[1024];
	sprintf(path,"server_dir/%s/history/.history",project);
	FILE* fp = fopen(path,"r");
	if(fp == NULL) {
		send(socket,(char*)&size,sizeof(int),0);
		printf("Project was not found\n");

		return -1;
	}
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0L,SEEK_SET);
	char buffer[size];
	fread(buffer,1,size,fp);
	fclose(fp);
	send(socket,(char*)&size,sizeof(int),0);
	send(socket,buffer,size,0);
	pthread_mutex_unlock(&table_lock);

	printf("Completed history\n");
	return 1;
}
