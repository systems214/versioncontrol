#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/types.h>
#include<errno.h>
#include<unistd.h>
#include<pthread.h>
#include<dirent.h>
#include"../helper_dir/helper.h"
#include"server.h"
#include"server_checkout.h"

int server_checkout(int socket,char* project, pthread_mutex_t * mtx){
	pthread_mutex_lock(&table_lock);



	int size = -1;
	if(mtx == NULL) {
		send(socket,(char*)&size,sizeof(int),0);
		printf("Project was not found\n");
		return -1;
	}
	size = -1;
	char path[1024] = {0};
	sprintf(path,"server_dir/%s/.manifest",project);
	FILE* fp = fopen(path, "r");
	if(fp == NULL) {
		send(socket,(char*)&size,sizeof(int),0);
		printf("Project was not found\n");

		return -1;
	}
	fclose(fp);
	char cmd[1024] = {0};
	sprintf(cmd,"tar -czvf server_dir/package.tar.gz -C server_dir/ %s",project);
	system(cmd);

	memset(cmd,'\0',1024);
	sprintf(cmd, "server_dir/package.tar.gz");
	FILE* fp2 = fopen(cmd, "r");
	fseek(fp2, 0L,SEEK_END);
	size = ftell(fp2);
	fseek(fp2, 0L, SEEK_SET);

	char buffer[size];
	fread(buffer,1,size,fp2);
	fclose(fp2);

	send(socket,(char*)&size,sizeof(int),0);
	send(socket,buffer,size,0);

	remove(cmd);


	pthread_mutex_unlock(&table_lock);

	printf("Completed checkout\n");
	return 1;
}
